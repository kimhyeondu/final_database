#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "database.h"
#include <QModelIndex>
#include <QAbstractTableModel>
#include <QStandardItem>
#include<QLoggingCategory>
#include <QFileDialog>
Q_LOGGING_CATEGORY( db,"test" )

database::database(QObject*parent):QObject(parent)
{
  qDebug() << "hello";
}




database* database::getInstance( QObject* parent )
{
    static database* instance = new database( parent );
    return instance;
}


QSqlDatabase database :: init_database(){

     QSqlDatabase db= QSqlDatabase::database(db_name);
     db.open();
    return db;


}


void database::delet_data(){
    QSqlQuery qy(init_database());
    qy.prepare( "DELETE FROM test "
                "WHERE num = "
                " :num ");
   qy.bindValue(":num",num_n-1);
    if(!qy.exec())
    {

        qDebug()<<qy.lastError();
        qDebug()<<qy.lastQuery();
        qDebug() << "delet error";
    }
   else
    {qDebug () <<"delet!!!";
    num_n -=1;}

}

void database :: create_database(QString Qstring_db_name){
     db_name=Qstring_db_name;
    QSqlDatabase db = QSqlDatabase::addDatabase( "QSQLITE", db_name );
    db.setDatabaseName( db_name +".db" );
     db.open();
     create_table();

}

void database::create_table()
{

    QSqlQuery qy(init_database());
    qy.prepare("CREATE TABLE IF NOT EXISTS test"
                       "("
                       "num      INTEGER  NOT NULL PRIMARY KEY,"
                       "temp     INTEGER NOT NULL,"
                       "humi     INTEGER NOT NULL,"
                       "rain     INTEGER NOT NULL,"
                       "image   BLOB"
                       ");");
    num_n =1;




    if(!qy.exec())
    {
        qDebug()<<qy.lastError();
        qDebug()<<qy.lastQuery();
        qDebug() << "creat error";
    }
    else
        qDebug() << "create!!!!";
////////////////////////////////////////////////////////

}



void database:: insert_database(QString tempp, QString humii, QString rainyy){


        QPixmap pixmap =image;

       // QByteArray image_array;
        //QBuffer image_buffer(&image_array);
     QByteArray image_array;
     QBuffer image_buffer(&image_array);
        image_buffer.open(QIODevice::WriteOnly);
        pixmap.save(&image_buffer,"PNG");

    QSqlQuery qy(init_database());
      qy.prepare("INSERT INTO test"
                 "('num' ,'temp','humi','rain','image')"
    //               "('num' ,'temp','humi','rain')"
                  "VALUES"
   //               "( ?, ?,?,?)");
                "(?, ?, ?,?,?)");
      qy.bindValue(0,num_n);
      qy.bindValue(1,tempp);
      qy.bindValue(2,humii);
      qy.bindValue(3,rainyy);
      qy.bindValue(4,image_array);

         num_n+=1;
      if(!qy.exec())
      {
          qDebug()<<qy.lastError();
          qDebug()<<qy.lastQuery();
          qDebug() << "insert error";
      }
      else

               qDebug () <<"inset!!!";

   }


/*
void database :: show_database(QTableView *Tableview){


    QStandardItemModel *standardModel = new QStandardItemModel();
    QSqlQuery query(init_database());
    query.prepare("SELECT * FROM test ORDER BY image");
    if(query.exec())
    while(query.next())
    {
        QStandardItem * item = new QStandardItem(query.value("num").toString());
       QStandardItem * item2 = new QStandardItem(query.value("temp").toString());
        QStandardItem * item3 = new QStandardItem(query.value("humi").toString());
         QStandardItem * item4 = new QStandardItem(query.value("rain").toString());

       QStandardItem * item5 = new QStandardItem(query.value("image").toString());
         item5->setData(QVariant(image),Qt::DecorationRole);
          QList<QStandardItem*> items;
       // standardModel->appendRow(item);
        items.append((item));
        items.append((item2));
        items.append((item3));
        items.append((item4));
        items.append((item5));
        standardModel->appendRow(items);

    }

        Tableview->setModel(standardModel);
    sqltab
    mode1= new QSqlTableModel(nullptr,init_database());
    mode1->setTable("test");
    mode1->setEditStrategy(QSqlTableModel::OnManualSubmit);
    mode1->select();
    Tableview->setModel(mode1);

*/




   // ------------------------------------------------------------------//
    //  QStandardItemModel *itemmodel = new QStandardItemModel;
     // QStandardItem *item = new QStandardItem();
     //     QStandardItem *item2 = new QStandardItem();
      //item ->setData(QVariant(image),Qt::DecorationRole);
    //  item2->setData(QVariant(QSqlTableModel::OnManualSubmit),Qt::DecorationRole);
     // itemmodel->setItem(0,0,item2);
     // itemmodel->setItem(0,1,item);
   //Tableview->setModel(itemmodel);
//----------------------------------------------------------------------------//


//}

QPixmap database :: load_img_File(){

   // QString img_path = QFileDialog::getExistingDirectory(nullptr,"search Folder",QDir::homePath(),QFileDialog::ShowDirsOnly);
    //QString img_path = QFileDialog::getOpenFileName(nullptr, "Open File",
      //                                              "/home",
        //                                            "Images (*.png *.xpm *.jpg)");
    QPixmap pixmap_image;
    pixmap_image.load(QFileDialog::getOpenFileName(nullptr, "Open File",
                                                   "/home",
                                                  "Images (*.png *.xpm *.jpg)"));

    image=pixmap_image;
  /*  QStringList file_name = QFileDialog::getOpenFileNames(
                            nullptr,
                            "Select one or more files to open",
                            "/home",
                            "Images (*.png *.xpm *.jpg)");
*/

    return pixmap_image;
}

database :: ~database(){


}





