#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "database.h"
#include <QMainWindow>
#include <QPushButton>
#include <QTextEdit>
#include <QtSql/QSqlDatabase>
#include "imagesqltablemodel.h"




QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void on_insert_button_clicked();
    void on_pushButton_clicked();
    void on_delet_button_clicked();
    void on_show_button_clicked();
    void on_img_button_clicked();
    void on_save_button_clicked();

 //  database *database_class = database::getInstance();

signals:
    void signals_insert_db();
private slots:





private:
    database* m_pManagerDataBase = database::getInstance();
    QSqlTableModel* model         = new ImageSqlTableModel( this, m_pManagerDataBase->init_database() );
    QSqlQueryModel* qryModel      = new ImageSqlTableModel( this, m_pManagerDataBase->init_database() );


    Ui::MainWindow *ui;

    //singleton inspection

};
#endif // MAINWINDOW_H
