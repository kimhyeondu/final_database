#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);



    connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::on_pushButton_clicked);
    connect(ui->insert_button, &QPushButton::clicked, this, &MainWindow::on_insert_button_clicked);
    connect(ui->delet_button, &QPushButton::clicked, this, &MainWindow::on_delet_button_clicked);
    connect(ui->show_button, &QPushButton::clicked, this, &MainWindow::on_show_button_clicked);
    connect(ui->img_button, &QPushButton::clicked, this, &MainWindow::on_img_button_clicked);
    connect(ui->save_button, &QPushButton::clicked, this, &MainWindow::on_save_button_clicked);


}





MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_insert_button_clicked()
{

database::getInstance()->insert_database( ui->temp_edit->toPlainText(), ui->hum_edit->toPlainText(), ui->rain_edit->toPlainText());

}


void MainWindow::on_pushButton_clicked()
{

database::getInstance()->create_database(ui->textEdit->toPlainText());

}

void MainWindow::on_delet_button_clicked()
{
    database::getInstance()->delet_data();
}

void MainWindow ::on_show_button_clicked(){

   ImageSqlTableModel  * mode1   =new ImageSqlTableModel(nullptr,database::getInstance()->init_database());
    mode1->setTable("test");
    mode1->setEditStrategy(QSqlTableModel::OnManualSubmit);
    mode1->select();
    ui->tableWidget->setModel(mode1);
}

void MainWindow::on_img_button_clicked()
{
   database::getInstance()->load_img_File();

}

void MainWindow::on_save_button_clicked()
{

}
